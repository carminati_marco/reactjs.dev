import React, { Component } from 'react';
import './App.css'
import { FormGroup, FormControl, InputGroup, Glyphicon } from 'react-bootstrap'
import Profile from './Profile'
import Gallery from './Gallery'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
      artist: null,
      tracks: []
    }
  }

  search() {
    const BASE_URL = 'https://api.spotify.com/v1/search';
    let FETCH_URL = `${BASE_URL}?q=${this.state.query}&type=artist&limit=1`;
    const ALBUM_URL = 'https://api.spotify.com/v1/artists/'
    const BEARER = "BQBcP9zmd62rjmBjo490dVG2b-t9FEHexsioQkQUhwq7JVDKMIhB1FIX0GU70KgQBfk02pYH_hprS2WRk5paNA"


    fetch(FETCH_URL, {
      method: 'GET',
      headers: { "Authorization": `Bearer ${BEARER}`
      }
    })
    .then(response => response.json())
    .then(json =>{
      const artist = json.artists.items[0];
      this.setState({artist});

      FETCH_URL = `${ALBUM_URL}${artist.id}/top-tracks?country=IT&`;

      fetch(FETCH_URL, {
        method: 'GET',
        headers: { "Authorization": `Bearer ${BEARER}`
        }
      })
      .then(response => response.json())
      .then(json =>{
          const {tracks} = json;
          this.setState({tracks});
      })
    })
  }

  render() {
    return (
      <div className="App">
        <div className="App-title" >Music Master App</div>

        <FormGroup>
          <InputGroup>
            <FormControl type="text" placeholder="cerca"
              value={this.state.query}
              onKeyPress={event => {if (event.key === 'Enter') { this.search();}}}
              onChange={event => {this.setState({query: event.target.value})}} />
            <InputGroup.Addon onClick={() => this.search()}>
              <Glyphicon glyph="search" ></Glyphicon>
            </InputGroup.Addon>
          </InputGroup>
        </FormGroup>
        {
          this.state.artist !== null
          ? <div>
          <Profile
            artist={this.state.artist}>
          </Profile>
          <Gallery tracks={this.state.tracks}>
          </Gallery>
          </div>
          : <div></div>
        }
      </div>
    )
  }
}
export default App;
