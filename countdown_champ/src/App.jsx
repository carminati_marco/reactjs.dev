import React, { Component } from 'react';
import Clock from './Clock';
import "./App.css";
import { Form, FormControl, Button} from 'react-bootstrap';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deadline: "December 25, 2017",
      newDeadline: "",
    }
  }

  changeDeadline() {
    console.log('state', this.state)
    this.setState({deadline: this.state.newDeadline });
  }

  render() {
    return (
      <div className="App" >
        <div className="App-title">Countdown { this.state.deadline }</div>
          <Clock deadline={this.state.deadline} ></Clock>
        <Form inline>
          <FormControl className="Deadline-input" placeholder="New date"
            onChange={event => this.setState({newDeadline: event.target.value})} ></FormControl>
          <Button onClick={() => this.changeDeadline()} >Submit</Button>
        </Form>
      </div>
    )
  }
}

export default App;
